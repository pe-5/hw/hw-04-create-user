function createNewUser(){

  let firstName = prompt('Enter your name:', 'Yevhenii')
  let lastName = prompt('Enter your last name:', 'Tarasevych')
  
  const newUser = {
    _firstName: firstName,
    _lastName : lastName,
  
    get firstName() {
      return this._firstName
    },

    set firstName (newName) {
      this._firstName = newName 
    },

    get lastName() {
      return this._lastName
    },

    set lastName (newLastName) {
      this._lastName = newLastName 
    },

    getLogin(){
      return (this.firstName[0] + this.lastName).toLowerCase()
    }
  } 
  
  return newUser
}

let user = createNewUser();
let login = user.getLogin();

console.log(login);
